from django.shortcuts import render
from status.forms import StatusForm
from status.models import StatusPost

# Create your views here.
def index(request):
	form = StatusForm(request.POST or None)
	response = {}
	response['form'] = form
	if(request.method == 'POST' and form.is_valid()):		
		response['status'] = request.POST['status']
		status = StatusPost(status=response['status'])
		status.save()
	status = StatusPost.objects.all().order_by('-id')
	response['status'] = status
	return render(request, 'index.html', response)