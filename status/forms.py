from django import forms
from status.models import StatusPost

class StatusForm(forms.ModelForm):
	status = forms.CharField(label='Status ', widget=forms.Textarea(attrs={'class': 'textarea'}))

	class Meta:
		model = StatusPost
		fields = {'status'}